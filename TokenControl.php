<?php

namespace HiMax;

!defined(__NAMESPACE__ . '\TABLE_NAME_TOKEN') 
        && define(__NAMESPACE__ . '\TABLE_NAME_TOKEN', constant(__NAMESPACE__ . '\TABLES_PREFFIX') . 'token');

/**
 * HiMax
 *
 * A toolkit implement to authenticate and authorize users actions
 *
 * @package     HiMax
 * @category	Security
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/himax
 */
trait TokenControl {
    
    
    /**
     * Capture info sent to be authenticated
     * @access protected
     * @return array Description
     */
    protected function getAuthInfo() {
        if(empty($this->info['login'])) {
            $tokenInfo = $this->getTokenInfo();

            $authInfo = !empty($tokenInfo) ? $tokenInfo : $this->_getAuthInfo();
                !empty($tokenInfo) && $this->getAttr('log')->add("Token Info")->add($authInfo);//->add(debug_backtrace(null, 5));
            $this->info['login'] = $authInfo;
        }
        return $this->info['login'];
    }
    
    abstract protected function _getAuthInfo();
    
    protected function getTokenInfo() {
        if (array_key_exists('token', $_REQUEST)) {
            return [$_REQUEST['token']];
        }
        return NULL;
    }
    
    public function authenticateBy($authInfo) {
        $auth = false;
        if(count($authInfo)) {
            $this->getAttr('log')->add("info received")->add($authInfo);
            if(count($authInfo)===1) {
                $auth = $this->authenticateToken($authInfo[0]);
                $auth===true && $this->addLoggedData('user', $user = $this->getUserFromToken()) && 
                $this->getAttr('log')->add("user data")->add($user);
            } else {
                $auth = $this->authenticateLogin($authInfo[0], $authInfo[1]);
                $auth===true && $this->addLoggedData('token', $token = $this->getTokenForUser()) &&
                    $this->getAttr('log')->add("token data")->add($token);
            }
        }
        
        return $auth;
    }
    
    /** Check token and capture token data
     * @access protected
     * @param token
     * @return mixed true for token and user found or code error
     */
    protected function authenticateToken($token) {
        $tokenData = $this->getToken($token);
        $this->getAttr('log')->add("token data")->add($tokenData);
        $this->addLoggedData('token', $tokenData);
        
        if (empty($token = $this->getData('token'))) {
            return \HiMax\Core::ERROR_TOKENNOTFOUND;
        }

        return true;
    }
    
    /**
     * Get user according to user id found on token
     * @access protected
     * @return array
     */
    protected function getUserFromToken() {
        if (!empty($this->getData('token'))) {
            $userData = $this->model['user']->login_getByToken($this->getData('token'));
            return $userData;
        }
    }
    
    /**
     * Get token into the table based on token, otoken, etoken or ctoken
     * @access protected
     * @param string $token
     * @return array
     */
    protected function getToken($token) {
        return !empty($token) ? $this->model['token']->login_getBy(['token' => $token]) : null;
    }

    /**
     * Get token according to user id
     * @access protected
     * @return array
     */
    public function getTokenForUser($userId = null) {
        $userId === null && !empty($this->getData('user')) && ($userId = $this->getData('user.id'));
        return !empty($userId) ? $this->model['token']->getForUserId($userId) : [];
    }
    
    public function eraseToken() {
        $this->model['token']->wipe($this->getData('token'), $this->getData('user'));
    }
    
    /**
     * Logout
     * @access protected
     */
    public function logout() {
        $this->eraseToken();
    }
    
}
