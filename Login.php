<?php

namespace HiMax;

/**
 * HiMax
 *
 * A toolkit implement to authenticate and authorize users actions
 *
 * @package     HiMax
 * @category	Security
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/himax
 */
trait Login {
    use \Singleton;

    public $frameworkBridge = [];
    public $frameworkInstance = [];
    public $info = [];
    
    public function initialize() {
        list($this->frameworkInstance, $this->frameworkBridge) = Core::getFrameworkBridge();
        !empty(@$_REQUEST['logout']) && ($this->logout());
    }
    
    /**
     * Capture info sent to be authenticated
     * @access protected
     * @return array Description
     */
    protected function getAuthInfo() {
        if(empty($this->info['login'])) {
            $httpInfo = $this->getHttpInfo();
            $postInfo = $this->getPostInfo();

            $authInfo = (!empty($httpInfo) ? $httpInfo : $postInfo);
                $this->getAttr('log')->add("Auth Info")->add($authInfo);
            $this->info['login'] = $authInfo;
        }
        return $this->info['login'];
    }
    
    protected function getHttpInfo() {
        if (!empty($_SERVER['PHP_AUTH_USER'])) {
            return [(string) @$_SERVER['PHP_AUTH_USER'], (string) @$_SERVER['PHP_AUTH_PW']];
        }
        return NULL;
    }
    
    protected function getPostInfo() {
        if (array_key_exists('login', $_REQUEST) && is_array($_REQUEST['login'])) {
            return [(string) @$_REQUEST['login']['user'], (string) @$_REQUEST['login']['pass']];
        }
        return NULL;
    }
    
    /**
     * Logout
     * @access protected
     */
    public function logout() {
        $this->eraseLoggedData();
        $this->output(\HiMax\Core::ERROR_NOTAUTHENTICATED);
    }
    
    
    /* separated control for data respective to user access control */
    public function addLoggedData($key, $data = []) {
        empty($this->getData('loggedData')) && $this->addData('loggedData', []);
        if(!in_array($key, $this->getData('loggedData'), true)) {
            $this->frameworkBridge->arrayApply('loggedData', 'array_push', [$key]);
        }
        return $this->addData($key, $data);
    }
    
    public function eraseLoggedData() {
        $loggedData = $this->getData('loggedData');
        if(!empty($loggedData))
        foreach($loggedData as $key) {
            $this->addData($key, []);
        }
        $this->addData('loggedData', []);
    }
    
    /* Session Data Methods */
    public function getData($key = null) {
        return $this->frameworkBridge->getData($key);
    }
    
    public function addData($key, $value, $stack = null) {
        return $this->frameworkBridge->addData($key, $value, $stack);
    }
    
    public function eraseData() {
        return $this->frameworkBridge->eraseData();
    }
    
    public function throwMessage($code, $title='') {
        $this->showMessage(htmlentities($this->getMessage($code)), $title);
    }
    
    abstract public function output();
    abstract public function getMessage();
    abstract public function showMessage();
}
