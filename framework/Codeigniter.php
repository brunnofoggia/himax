<?php

namespace HiMax\framework;

/**
 * HiMax
 *
 * A toolkit implement to authenticate and authorize users actions
 *
 * @package     HiMax
 * @category	Security
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/himax
 */
class Codeigniter {

    use \codeigniter\CodeBlaze\Serialize;

    public function __construct() {
        $this->unserialize();
    }
    
    /**
     * Generate UID
     * @access protected
     * @return string
     */
    protected function generateUID() {
        $uid = \HiMax\Core::SESSIONPREFFIX . '-' . session_id();

        return $uid;
    }
    
    /**
     * Fetch route called
     * @access protected
     * @return array ['module'=>'','controller'=>'','action'=>'','route'=>'']
     */
    public function getRequestInfo() {
        $ci = \get_instance();
        $data = [
            'module' => $this->routerFetchModule(),
            'controller' => $ci->router->fetch_class(),
            'action' => $ci->router->fetch_method(),
            'route' => !empty($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : (!empty($_SERVER['REQUEST_URI']) ? explode('?', $_SERVER['REQUEST_URI'], 2)[0] : null)
        ];

        return $data;
    }

    /**
     * Discover ci module/dir name for controller called
     * @return string
     */
    public function routerFetchModule() {
        $ci = \get_instance();
        if (method_exists($ci->router, 'fetch_module')) {
            return $ci->router->fetch_module();
        } elseif (method_exists($ci->router, 'fetch_directory')) {
            $directory = preg_replace('/\/$/', '', $ci->router->fetch_directory());
            return !empty($directory) ? $directory : null;
        }
    }
    
    protected function readConfig($name) {
        $ci = \get_instance();
        return $ci->config->config[$name];
    }
}
