<?php

namespace HiMax;

!defined(__NAMESPACE__ . '\TABLE_NAME_ACL')
        && define(__NAMESPACE__ . '\TABLE_NAME_ACL', constant(__NAMESPACE__ . '\TABLES_PREFFIX') . 'acl');
!defined(__NAMESPACE__ . '\TABLE_NAME_ACTION') 
        && define(__NAMESPACE__ . '\TABLE_NAME_ACTION', constant(__NAMESPACE__ . '\TABLES_PREFFIX') . 'action');
!defined(__NAMESPACE__ . '\TABLE_NAME_PROFILE') 
        && define(__NAMESPACE__ . '\TABLE_NAME_PROFILE', constant(__NAMESPACE__ . '\TABLES_PREFFIX') . 'profile');
!defined(__NAMESPACE__ . '\TABLE_NAME_ACCESS') 
        && define(__NAMESPACE__ . '\TABLE_NAME_ACCESS', constant(__NAMESPACE__ . '\TABLES_PREFFIX') . 'access');

/**
 * HiMax
 *
 * A toolkit implement to authenticate and authorize users actions
 *
 * @package     HiMax
 * @category	Security
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/himax
 */
trait AclControl {
    use DBAuthControl {
        DBAuthControl::authorize as _authorize;
    }

    protected $aclAttrDefaults = [
    ];
    
    protected function getAttrProperty() {
        return $this->aclAttrDefaults;
    }
    
    public function authorize() {
        if(\HiMax\Core::getMe()->getAttr('controlLevel') === \HiMax\Core::CONTROL_NONE) return true;
        
        $requestInfo = $this->frameworkBridge->getRequestInfo();
        /* public access || private/internal access & already logged */
        $this->storeAcl(\HiMax\Core::ACTION_PUBLIC, $this->getPublicAcl());
        $checkAccess = $this->checkAccess(@$requestInfo['controller'], @$requestInfo['action'], @$requestInfo['module'], @$requestInfo['route']);
        if(!($this->getAuthInfo())) {
            return $checkAccess;
        }
        
        // not logged in. try to login , read permissions and concede access
        $this->eraseLoggedData();
        /* private / internal access not yet logged */
        $auth = $this->_authorize();
        if(\HiMax\Core::getMe()->getAttr('controlLevel') !== \HiMax\Core::CONTROL_ACL) {
            return $auth;
        }
        
        $this->getAcl();
        $checkAccess = $this->checkAccess(@$requestInfo['controller'], @$requestInfo['action'], @$requestInfo['module'], @$requestInfo['route']);
        return $checkAccess;
    }

    /**
     * Organize action control list
     * @param bool $renew
     * @access protected
     */
    protected function getAcl() {
        $this->storeAcl(\HiMax\Core::ACTION_PRIVATE, $this->getPrivateAcl());
        $this->storeAcl(\HiMax\Core::ACTION_INTERNAL, $this->getInternalAcl());
    }
    
    protected function storeAcl($key, $data) {
        if((string) $key === (string) \HiMax\Core::ACTION_PUBLIC) {
            return $this->addData('acl.'.$key, $data);
        }
        return $this->addLoggedData('acl.'.$key, $data);
    }
    
    /**
     * Capture actions that can be accessed by beeing logged
     * @access protected
     * @return array actions list
     */
    protected function getInternalAcl() {
        $actionList = $this->model['action']->getInternalAcl();
        return $actionList;
    }

    /**
     * Capture actions that can be accessed anyway
     * @access protected
     * @return array actions list
     */
    protected function getPublicAcl() {
        $actionList = $this->model['action']->getPublicAcl();
        return $actionList;
    }

    /**
     * Capture actions that can be accessed only authorized users
     * @access protected
     * @return array actions list
     */
    protected function getPrivateAcl() {
        $actionList = [];

        if (!empty($this->getData('user'))) {
            $actionIdList = $this->model['access']->extractActionIdList($this->getData('user'));
            
            count($actionIdList) && $actionList = $this->_callGetPrivateAcl($actionIdList);
        }
        
        return $actionList;
    }
    
    protected function _callGetPrivateAcl($actionIdList) {
        return $this->model['action']->getPrivateAcl($actionIdList);
    }
    
    /**
     * Get action data
     * @param string $class
     * @param string $action
     * @param string $module
     * @param string $route
     * @param string $sys
     * @return array
     */
    public function getActionData($class, $action, $module, $route = null) {
        empty($route) && ($route = implode('/', [$module, $class, $action]));
        $route = \Himax\Core::formatRoute($route);

        $actionList = $this->model['action']->getData($class, $action, $module, $route);
        return $actionList;
    }

    /**
     * Check privacy of the action called and if the guest is authorized to run it
     * @param string $class
     * @param string $action
     * @param string $module
     * @param string $route
     * @param string $sys
     * @return mixed true for authorized otherwise code error
     */
    public function checkAccess($class, $action, $module, $route = null, $actionData = null) {
        if(\HiMax\Core::getMe()->getAttr('controlLevel') !== \HiMax\Core::CONTROL_ACL) return true;
        
        ($actionData===null) && ($actionData = $this->locateAction($class, $action, $module, $route));
        /* not located. means the action is not mapped or this user does not have access to it */
        if (empty($actionData)) {
            $actionData = $this->getActionData($class, $action, $module, $route);
            if(!empty($actionData)) {
                $this->getAttr('log')->add("action called")->add(func_get_args());
                $this->getAttr('log')->add("data loaded")->add($this->getData());
            return $this->isLogged() ? \HiMax\Core::ERROR_FORBIDDEN : \HiMax\Core::ERROR_NOTAUTHENTICATED;
            }
        } else if ($this->model['action']->checkAccessValue($actionData['action'], (string) \HiMax\Core::ACTION_PUBLIC)) {
            return true;
        } else if (!$this->isLogged()) {
            return \HiMax\Core::ERROR_NOTAUTHENTICATED;
        }
        return true;
    }

    /**
     * Locate an action based on controller+action+module* || route
     * @access protected
     * @param controller
     * @param action
     * @param module
     * @param route
     * @return array
     */
    protected function locateAction($class, $action, $module, $route) {
        $actionData = [];
        $privateAction = $internalAction = $publicAction = null;
        $route = \HiMax\Core::formatRoute($route);
        
        $callback = function($item) use($class, $action, $module, $route) {
            return \HiMax\Core::filterAction($item, $class, $action, $module, $route);
        };

        $privateAction = array_filter((array) @$this->getData('acl.'.\HiMax\Core::ACTION_PRIVATE), $callback);
        !empty($privateAction) && $actionData = ['action' => array_shift($privateAction), 'privacy' => \HiMax\Core::ACTION_PRIVATE];

        empty($actionData) && $internalAction = array_filter((array) @$this->getData('acl.'.\HiMax\Core::ACTION_INTERNAL), $callback);
        !empty($internalAction) && $actionData = ['action' => array_shift($internalAction), 'privacy' => \HiMax\Core::ACTION_INTERNAL];

        empty($actionData) && $publicAction = array_filter((array) @$this->getData('acl.'.\HiMax\Core::ACTION_PUBLIC), $callback);
        !empty($publicAction) && $actionData = ['action' => array_shift($publicAction), 'privacy' => \HiMax\Core::ACTION_PUBLIC];
        return $actionData;
    }

    /**
     * Locate granted access to an action, if granted
     * @param array $actionList
     */
    public function locateAccess($actionList) {
        $actionPk = $this->model['action']->getAttr('primaryKey');
        $profilePk = $this->model['profile']->getAttr('primaryKey');
        
        $actionList = \Crush\Collection::transform($actionList, $actionPk);
        $actionIdList = \Crush\Collection::transform($actionList, '', [$actionPk], ['flatten']);
        $profileIdList = \Crush\Collection::transform((array) @$this->getData('user')['acl'], '', ['profile.'.$profilePk], ['flatten']);

        if(!empty($profileIdList)) {        
            /* for public & internal actions */
            foreach($actionList as $action) {
                if(!$this->model['action']->checkAccessValue($action, \HiMax\Core::ACTION_PRIVATE)) {
                    return $action;
                }
            }

            /* private actions */
            $result = $this->model['access']->locate($actionIdList, $profileIdList);
            if(!empty($result)) {
                return $actionList[array_shift($result)];
            }
        }
        return false;
    }

    /**
     * Get the first item from list of profile or department
     * @param type $list [profileId|departmentId]
     * @return string
     */
    public function getMainItem($list='') {
        if($this->isLogged()===true && !empty($this->getData('user')[$list])) {
            return array_shift($this->getData('user')[$list]);
        }
        return '';
    }
    
    /**
     * Get first profile
     * @return string
     */
    public function getMainProfile() {
        return $this->getMainItem('profileId');
    }
    
    /**
     * Get first department
     * @return string
     */
    public function getMainDepartment() {
        return $this->getMainItem('departmentId');
    }
    
    /**
     * Checks if any of logged user profiles match with the one given
     * @param string $id
     * @return boolean
     */
    public function userHasProfile($id) {
        if($this->isLogged()===true) {
            $loggedGroupInfo = $this->getData('user')['profileId'];
            if(!empty($id)) foreach($loggedGroupInfo as $groupId) if ((string) $id === (string) $groupId) return true;
        }
        return false;
    }
    
    /**
     * Checks if any of logged user departments match with the one given
     * @param string $id
     * @return boolean
     */
    public function userHasDepartment($id) {
        if($this->isLogged()===true) {
            $loggedDepartmentInfo = $this->getData('user')['departmentId'];
            if(!empty($id)) foreach($loggedDepartmentInfo as $departmentId) if ((string) $id === (string) $departmentId) return true;
        }
        return false;
    }

    /**
     * Verifies if logged user belongs to a profile that is on a higher level (is_admin) than the given level
     * @param integer $level
     * @return boolean
     */
    public function isHigherGroupThanLevel($level) {
        $loggedGroupInfo = $this->isLogged()===true ? $this->getData('user')['profile'] : [];
        
        if($level !== '0' && empty($level)) return true;
        
        if(!empty($loggedGroupInfo))
            foreach($loggedGroupInfo as $group) if ((int) $group['is_admin'] > (int) $level || (int) $group['is_admin'] === 9) return true;
        return false;
    }

    /**
     * Check if logged user belongs to the profile owner of the record
     * @param mixed $id
     * @return boolean
     */
    public function isGroupOwner($groupId) {
        return $this->userHasProfile($groupId);
    }
    
    /**
     * Verifies if logged user belongs to the same profile than the record and if the logged user is on a higher level (is_admin) than the record owner (supervisor)
     * @param integer $level
     * @return boolean
     */
    public function isHigherUserOnSameGroupThan($owner, $group) {
        if(!$this->isGroupOwner($group)) return false;
        $owner = \HiMax\Core::getMe()->getModels()['user']->getBy(['user'=>$owner], ['user', 'is_admin']);
        $loggedUser = $this->getData('user');
        
        if ((int) $loggedUser['is_admin'] > (int) $owner['is_admin'] || (int) $loggedUser['is_admin'] === 9) return true;
    }

}
