<?php

namespace HiMax;

class GenericShield {
    use \HiMax\AuthControl;
    protected $model = [];
    
    public function output() {}
    public function getMessage() {}
    public function showMessage() {}
    
//    
//    protected static $errorMsg = [
//        0 => 'Erro não identificado.',
//        \HiMax\Core::ERROR_TOKENNOTFOUND => 'Sua sessão expirou.',
//        \HiMax\Core::ERROR_USERNOTFOUND => 'Usuário não encontrado.',
//        \HiMax\Core::ERROR_INVALIDPASSWORD => 'Senha incorreta.',
//        \HiMax\Core::ERROR_NOTAUTHENTICATED => 'Não autenticado.',
//        \HiMax\Core::ERROR_FORBIDDEN => 'Acesso Negado.',
//        \HiMax\Core::ERROR_INTERNAL => 'Erro interno.',
//        \HiMax\Core::ERROR_CONFIGDATANOTFOUND => 'Dados do sistema não encontrados no painel.',
//    ];
//    
//    public function logout() {
//        $this->token_logout();
//        $this->acl_logout();
//    }
//    
//    public function initialize() {
//        $this->model['system'] = \acsp\helpers\Util::loadDashesModel('\acsp\shield\Models\System', false);
//        $this->model['user'] = \acsp\helpers\Util::loadDashesModel('\acsp\shield\Models\User', false);
//        $this->model['profile'] = \acsp\helpers\Util::loadDashesModel('\acsp\shield\Models\Profile', false);
//        $this->model['department'] = \acsp\helpers\Util::loadDashesModel('\acsp\shield\Models\Department', false);
//        $this->model['token'] = \acsp\helpers\Util::loadDashesModel('\acsp\shield\Models\Token', false);
//        $this->model['acl'] = \acsp\helpers\Util::loadDashesModel('\acsp\shield\Models\Acl', false);
//        $this->model['access'] = \acsp\helpers\Util::loadDashesModel('\acsp\shield\Models\Access', false);
//        $this->model['action'] = \acsp\helpers\Util::loadDashesModel('\acsp\shield\Models\Action', false);
//        
//        $this->_initialize();
//        
//        if (empty($this->getData('system'))) {
//            $this->addData('system', $this->model['system']->getBy(['alias' => $this->getSystemAlias()]));
//        }
//    }
//    
//    /**
//     * Get token into the table based on token, otoken, etoken or ctoken
//     * @access protected
//     * @param string $token
//     * @return array
//     */
//    protected function getToken($token) {
//        if (empty($token) && !array_key_exists('token', $_REQUEST)) {
//            if (array_key_exists('otoken', $_REQUEST)) {
//                $conditions = ['id' => base64_decode(urldecode($_REQUEST['otoken']))];
//            } else if (array_key_exists('etoken', $_REQUEST)) {
//                $conditions = ['email' => base64_decode(urldecode($_REQUEST['etoken']))];
//            } else if (array_key_exists('ctoken', $_REQUEST)) {
//                $conditions = ['cpf' => base64_decode(urldecode($_REQUEST['ctoken']))];
//            }
//
//            $userData = $this->model['user']->getBy($conditions, [$this->model['user']->getAttr('primaryKey')], -1);
//            if (empty(@$userData[$this->model['user']->getAttr('primaryKey')])) {
//                return \HiMax\Core::ERROR_USERNOTFOUND;
//            }
//            $token = $this->model['token']->insert($userData[$this->model['user']->getAttr('primaryKey')]);
//        }
//        return $this->_getToken($token);
//    }
//
//    /**
//     * Get User List
//     * @param type $criteria
//     * @param type $fields
//     * @param type $acl [controller=>'', action=>'', module=>'']
//     * @param type $sys
//     * @return type
//     */
//    public function getUserListByACL($criteria, $fields = null, $acl = [], $sys = null) {
//        return $this->model['user']->getListByACL($criteria, $fields, $acl, $sys);
//    }
//    
//    public function output($code = null) {
//        if($code===true || (is_array(@$_REQUEST['login']) && !empty(@$_REQUEST['login']['info']))) {
//            $r = ['status' => true, 'code'=> @$this->info['auth']===true ? 200 : (int)@$this->info['auth']];
//            if (is_array(@$_REQUEST['login']) && !empty(@$_REQUEST['login']['info'])) {
//                if($_REQUEST['login']['info'] === 2) return $r;
//                die(json_encode($r));
//            }
//            return @$this->info['auth']===true;
//        } else {
//            $cpanelUrl = $this->getRedirUrl();
//            if (\Crush\Basic::isAjaxRequest()) {
//                $url = !empty($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : (string) @$_SERVER['REDIRECT_URL'];
//                header('HTTP/1.1 '.(is_int($code) && !empty($code) ? (int)$code : 500));
//                die(json_encode(['code' => $code, 'msg' => static::$errorMsg[$code].(!empty($url) ? ' ('.$url.')' : ''), 'login'=>$cpanelUrl]));
//            } else {
//                switch ($code) {
//                    case \HiMax\Core::ERROR_NOTAUTHENTICATED:
//                        \redirect($cpanelUrl);
//                        break;
//                    default:
//                        $addMsg = '';
//                        if(in_array($code, [498])) {
//                            $addMsg = '<script>setTimeout(function() { window.location.href = "'.$cpanelUrl.'" }, 5000);</script>';
//                        }
//                        $addMsg = ' <a href="'.$cpanelUrl.'">Voltar ao Painel</a>' . $addMsg;
//
//                        $this->showMessage(htmlentities($this->getMessage($code)) . $addMsg, 'Erro encontrado');
//                        die;
//                        break;
//                }
//            }
//        }
//    }
//    
//    public function getRedirUrl() {
//        /* poderá ser substituido por url customizada de login para o sistema em questao */
//        $cpanelUrl = \acsp\helpers\Url::ambienteUrl('http://' . \ACSP_URL_PAINEL_FRONT);
//        $cpanelLogoutUrl = \acsp\helpers\Url::ambienteUrl('http://' . \ACSP_URL_PAINEL_FRONT) . '?logout=1';
//        
//        if(strpos($cpanelUrl, $_SERVER['HTTP_HOST'])!==false) {
//            return $cpanelUrl;
//        }
//        return $cpanelLogoutUrl;
//    }
//    
//    public function getMessage($code) {
//        return @static::$errorMsg[(int) $code];
//}
//    
//    public function showMessage($msg, $title) {
//        echo '<div style="margin: 2rem 1.5rem; border: 1px solid #CCC; font-family: Verdana; font-size: .9em;">'. (!empty($title) ? '<h3 style="padding: 1em; margin: 0; border-bottom: 1px solid #CCC">'.$title.'</h3>' : '') . '<p style="margin: 1em;">'.$msg.'</p></div>';
//    }
}
