<?php

namespace HiMax;

!defined(__NAMESPACE__ . '\TABLE_NAME_FLOW') 
        && define(__NAMESPACE__ . '\TABLE_NAME_FLOW', 'flow');
!defined(__NAMESPACE__ . '\TABLE_NAME_FLOW_STEP') 
        && define(__NAMESPACE__ . '\TABLE_NAME_FLOW_STEP', 'flow_step');
!defined(__NAMESPACE__ . '\TABLE_NAME_FLOW_STEP_PLAY') 
        && define(__NAMESPACE__ . '\TABLE_NAME_FLOW_STEP_PLAY', 'flow_step_play');
!defined(__NAMESPACE__ . '\TABLE_NAME_FLOW_STEP_CHILD') 
        && define(__NAMESPACE__ . '\TABLE_NAME_FLOW_STEP_CHILD', 'flow_step_child');
/**
 * HiMax
 *
 * 
 *
 * @package     HiMax
 * @category	Security
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/himax
 */


trait FlowRecord {
    
    /**
     * Applies record control events to the model that requires it
     * @example description Should be applied before parent events on constructor
     * @return type
     */
    public function getFlowRecordEvents() {
        $events = [];
        
        $afterSavePersistAction = function($event, &$data) {
            $flowRecordConf = $this->getAttr('flowRecord');
            if(is_array($flowRecordConf) && !empty($flowRecordConf['uniqid']) && !empty($flowRecordConf['set_initial_step'])) {
                if(empty($flowRecordConf['initial_step_unidiq'])) {
                    $initialSteps = $this->Flow_getInitialSteps($flowRecordConf['uniqid']);
                    if(!count($initialSteps)) {
                        return;
                    }
                    $initialStep = array_shift($initialSteps);
                } else {
                    $initialStep = $this->Flow_step_get($flowRecordConf['initial_step_unidiq']);
                }
                
                if(empty($initialStep)) return;
                
//                $flowRecordModel = $this->loadModel('\HiMax\model\Flow');
                $flowPlayData = ['table'=>$this->getAttr('table'), 'table_id'=>$data[$this->getAttr('primaryKey')]];
                $flowPlayData['flow_step_uniqid'] = $initialStep['uniqid'];
                $flowPlayData['sys_user'] = @\HiMax\Core::getMe()->getData('user')['id'];
                
                $this->Flow_step_play_create($flowPlayData);
            }
        };
        
        $events[] = [['after', 'create'], $afterSavePersistAction];
        
        return $events;
    }
    
    public function __call($name, $arguments) {
        if(preg_match('/^Flow(_(step|step_play))?(_([a-zA-Z0-9]+))+$/', $name)) {
            $call = explode('_', $name);
            $method = array_pop($call);
            if(preg_match('/^idx/', $method)) { // to simulate method names to every step and apply security automatically
                $id = substr($method, 3);
                $method = array_pop($call);
                array_push($arguments, $id);
            }
            $class = implode('_', $call);
            
            if($class === 'Flow_step_play') {
                $arguments[] = $this->getAttr('table');
            }
            
//            printf("<pre>%s</pre>\n<br>", var_export($class, true));
//            printf("<pre>%s</pre>\n<br>", var_export($method, true));
//            printf("<pre>%s</pre>\n<br>", var_export($arguments, true));die;
            
            $model = $this->loadModel('\HiMax\model\\'.$class);
            if(method_exists($model, $method)) {
                $this->dispatchEvent(['before', $class . '-' . $method], ['arguments' => $arguments]);
                $exec = call_user_func_array([$model, $method], (array) @$arguments);
                $this->dispatchEvent(['after', $class . '-' . $method], ['arguments' => $arguments,'results'=>$exec]);
                return $exec;
            }
        }
    }
    
}
