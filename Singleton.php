<?php

namespace HiMax;
/**
 * trait Singleton
 */
trait Singleton {

    /**
     * @var Singleton reference to singleton instance
     */
    protected static $instance;
    
    /**
     * gets the instance via lazy initialization (created on first usage)
     *
     * @return self
     */
    final public static function getInstance() {
        if (null === static::$instance) {
            static::$instance = new static;
        }
        return static::$instance;
    }

    final public function __construct() {
        if (!empty(static::$instance)) {
            throw new \Exception('instance already loaded');
        }

        static::$instance = $this;
        method_exists($this, 'initialize') && call_user_func_array([$this, 'initialize'], func_get_args());
    }
    
    /**
     * alias for getInstance
     */
    final public static function getMe() {
        return static::getInstance();
    }

    /**
     * prevent the instance from being cloned
     *
     * @return void
     */
    protected function __clone() {}

    /**
     * prevent from being unserialized
     *
     * @return void
     */
    protected function __wakeup() {}

}