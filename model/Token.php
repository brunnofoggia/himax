<?php

namespace HiMax\model;

class Token {

    use \doctrine\Dashes\Model;
    
    protected $modelAttrDefaults = [
        'table' => \HiMax\TABLE_NAME_TOKEN,
        'deactivate' => false,
        'createdExpDays' => false,
        'foreignKeys' => [
            'user' => [
                'type' => \doctrine\Dashes\BELONGSTO,
                'key' => 'user_id',
                'model' => '\HiMax\model\User'
            ],
        ]
    ];
    
    public function login_getBy($conditions = [], $columns = null, $orderby = null, $recursive = null) {
        if($this->getAttr('createdExpDays')) {
            $valid = $this->getAttr('createdExpDays');
            
            $validDate = date('Y-m-d H:i', strtotime('-'.$valid.' days'));
            $conditions['created >='] = $validDate;
        }
        
        $result = $this->getBy($conditions, $columns, $orderby, $recursive);
        return $result;
    }

    public function insert($userId, $token = null) {
        $aliases = $this->getAliasesFields();
        $token === null && ($token = $this->generate($userId));
        $id = $this->create([$aliases['token'] => $token, $aliases['user_id'] => $userId]);
        return !empty($id) ? $token : null;
    }

    public function clear($userId) {
        $aliases = $this->getAliasesFields();
        !empty($userId) && $this->deleteAll([$aliases['user_id'] => $userId]);
    }
    
    public function getForUserId($userId) {
        $aliases = $this->getAliasesFields();
        $tokenData = !empty($userId) ? $this->getBy([$aliases['user_id'] => $userId]) : null;
        if (empty($tokenData)) {
            $token = $this->insert($userId);
            return !empty($token) ? $this->getBy([$aliases['token'] => $token]) : null;
        }
        return $tokenData;
    }
    
    public function wipe($token, $user) {
        $aliases = $this->getAliasesFields();
        if(!empty(@$token[$aliases['token']])) {
            $this->deleteBy([$aliases['token']=>$token[$aliases['token']]]);
        }
    }
    
    public function generate($userId) {
        return $userId . '#' . uniqid(rand() + time());
    }
}
