<?php

namespace HiMax\model;

class Access {
    use \doctrine\Dashes\Model;
    protected $modelAttrDefaults = [
        'table' => \HiMax\TABLE_NAME_ACCESS,
        'deactivate' => \HiMax\TABLES_FIELD_DELETE,
        'foreignKeys' => [
            'action' => [
                'type' => \doctrine\Dashes\BELONGSTO,
                'key' => 'action_id',
                'model' => '\HiMax\model\Action'
            ],
        ]
    ];
    
    public function extractActionIdList($user) {
        $profileIdList = (array) @$user['profileId'];
        $actionIdList = [];
        if(!empty($profileIdList)) {
            $accessList = count($profileIdList) ? $this->find(['profile_id' => $profileIdList], null, null, ['id', 'profile_id', 'action_id']) : [];
            $actionIdList = array_map(function($item) {
                return $item['action_id'];
            }, $accessList);
        }
        return $actionIdList;
    }
    
    public function getByProfileId_Action($action, $profileId) {
        $conditions = ['action_id'=>$action['id']];
        $conditions['profile_id'] = $profileId;
        $access = !empty($profileId) ? $this->getBy($conditions) : [];
        
        return $access;
    }
    
    public function locate($actionId, $profileId) {
        $results = [];
        
        if(!empty($actionId)) {
            $conditions = [];
            $conditions['action_id'] = $actionId;
            $conditions['profile_id'] = $profileId;

            $results = \Crush\Collection::transform($this->find($conditions), '', ['action_id'], ['flatten']);
        }
        return $results;
    }
}
