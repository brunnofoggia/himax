<?php

namespace HiMax\model;

class Control_record_share {
    use \doctrine\Dashes\Model {
        \doctrine\Dashes\Model::save as _save;
        \doctrine\Dashes\Model::create as _create;
        \doctrine\Dashes\Model::update as _update;
    }
    
    protected $modelAttrDefaults = [
        'table' => \HiMax\TABLE_NAME_CONTROL_RECORD_SHARE,
        'foreignKeys' => [
        ]
    ];
    
    public function save($data) {
        if(!empty($data['access']) && is_array($data['access'])) {
            $accessTotal = 0;
            foreach($data['access'] as $access)
                $accessTotal += (int) $access;
            
            $data['access'] = $accessTotal;
        }
        
        return $this->_save($data);
        
    }
    
    public function create($data) {
        $existsCriteria = $data;
        if(!empty($existsCriteria['delete'])) return;
        
        /* used to reuse rows when turning to default records */
        unset($existsCriteria['access']);
        unset($existsCriteria['delete']);
        if(empty($existsCriteria['share_id'])) unset($existsCriteria['share_id']);
        
        $existentRow = $this->getBy($existsCriteria);
        if(!empty($existentRow)) {
            $existentRow = array_merge($existentRow, $data);
            return $this->update($existentRow[$this->getAttr('primaryKey')], $existentRow);
        }
        
        if(array_key_exists('delete', $data)) unset($data['delete']);
        /**/
        
        return $this->_create($data);
    }
    
    public function update($id, $data) {
        if(!empty($data['delete'])) {
            if(!empty($data[$this->getAttr('primaryKey')])) {
                return $this->delete($data[$this->getAttr('primaryKey')]);
            }
            return true;
        } else if(array_key_exists('delete', $data)) unset($data['delete']);
        
        return $this->_update($data[$this->getAttr('primaryKey')], $data);
    }
}
