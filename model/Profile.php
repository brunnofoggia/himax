<?php

namespace HiMax\model;

class Profile {
    use \doctrine\Dashes\Model {
        \doctrine\Dashes\Model::findAll as _findAll;
    }
    protected $modelAttrDefaults = [
        'table' => \HiMax\TABLE_NAME_PROFILE,
        'deactivate' => \HiMax\TABLES_FIELD_DELETE,
        'status' => \HiMax\TABLES_FIELD_STATUS,
        'SystemAclControl' => false,
        'ignoreSystemFilter' => true,
        'foreignKeys' => [
            'access' => [
                'type' => \doctrine\Dashes\HASMANY,
                'key' => 'profile_id',
                'model' => '\HiMax\model\Access'
            ],
            'system' => [
                'type' => \doctrine\Dashes\BELONGSTO,
                'key' => 'system_id',
                'model' => '\HiMax\model\System'
            ],
        ]
    ];

    public function findAll($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $systemFk = $this->getAttr('foreignKeys')['system'];
        $systemModel = $this->loadFkModel('system');
        $systemData = \HiMax\Core::getMe()->getData('system');
        if(!empty($systemData) && !empty($this->getAttr('SystemAclControl')) && empty($this->getAttr('ignoreSystemFilter'))) {
            $systemCondition = [];
            $systemCondition[] = $this->getFieldName($systemFk['key']) . ' = ' . "'" . $systemData[$systemModel->getAttr('primaryKey')] . "'";
//            $systemCondition[] = $this->getFieldName($systemFk['key']) . ' = ' . "'0'";
            $conditions[] = '(' . implode(' OR ', $systemCondition) . ')';
//            $conditions[$systemFk['key']] = $systemData[$systemModel->getAttr('primaryKey')];
        }
        
        return $this->_findAll($conditions, $limit, $page, $columns, $orderby, $recursive);
    }
}
