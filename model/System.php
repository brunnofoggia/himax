<?php

namespace HiMax\model;

class System {
    
    use \doctrine\Dashes\Model;
    protected $modelAttrDefaults = [
        'table' => \HiMax\TABLE_NAME_SYSTEM,
        'deactivate' => \HiMax\TABLES_FIELD_DELETE,
        'foreignKeys' => [
        ]
    ];
    
    public function getSystemList() {
        $systemList = $this->find();
        
        $results = [];
        foreach($systemList as $x => $systemItem) {
            $systemItem['url'] = $this->formatSystemUrl($systemItem['url'], true);
            if(!empty($systemItem['route'])) {
                @list($module, $controller, $action) = explode('/', $systemItem['route']);
                if(\HiMax\Core::getMe()->checkAccess($controller, $action, $module, null, $systemItem['id'])===true) {
                    $results[$systemItem['name']] = $systemItem;
                }
            }
        }
        ksort($results);
        return $results;
    }
}
