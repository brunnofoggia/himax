CREATE TABLE control_record_share (
  id                int(11) NOT NULL AUTO_INCREMENT, 
  control_record_id int(11) NOT NULL, 
  share             tinyint(1) DEFAULT 0 comment 'owner, group, public, department', 
  share_id          varchar(255), 
  access            tinyint(1) DEFAULT 0 NOT NULL comment 'read, write, execute', 
  PRIMARY KEY (id)) engine=InnoDB;
CREATE TABLE control_record (
  id       int(11) NOT NULL AUTO_INCREMENT, 
  `table`  varchar(255) NOT NULL, 
  table_id varchar(255) NOT NULL, 
  owner    varchar(255) DEFAULT '0', 
  `group`  varchar(255), 
  `default`  varchar(255), 
  PRIMARY KEY (id)) engine=InnoDB;
