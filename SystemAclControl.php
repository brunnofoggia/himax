<?php

namespace HiMax;

!defined(__NAMESPACE__ . '\TABLE_NAME_SYSTEM') 
        && define(__NAMESPACE__ . '\TABLE_NAME_SYSTEM', constant(__NAMESPACE__ . '\TABLES_PREFFIX') . 'system');

/**
 * HiMax
 *
 * A toolkit implement to authenticate and authorize users actions
 *
 * @package     HiMax
 * @category	Security
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/himax
 */
trait SystemAclControl {
    use AclControl {
        AclControl::getAcl as _getAcl;
        AclControl::getInternalAcl as _getInternalAcl;
        AclControl::getPublicAcl as _getPublicAcl;
        AclControl::getPrivateAcl as _getPrivateAcl;
        AclControl::getActionData as _getActionData;
        AclControl::checkAccess as _checkAccess;
    }
    
    protected $systemAclAttrDefaults = [
    ];
    
    protected function getAttrProperty() {
        return array_merge($this->aclAttrDefaults, $this->systemAclAttrDefaults);
    }
    
    protected function getAcl() {
        if(empty($this->getSystemId())) {
            $this->throwMessage(\HiMax\Core::ERROR_CONFIGDATANOTFOUND);die;
        }
        return $this->_getAcl();
    }
    
    /**
     * Capture actions that can be accessed by beeing logged
     * @access protected
     * @return array actions list
     */
    protected function getInternalAcl() {
        $actionList = $this->model['action']->getInternalAcl($this->getSystemId());
        return $actionList;
    }

    /**
     * Capture actions that can be accessed anyway
     * @access protected
     * @return array actions list
     */
    protected function getPublicAcl() {
        $actionList = $this->model['action']->getPublicAcl($this->getSystemId());
        return $actionList;
    }
    
    protected function _callGetPrivateAcl($actionIdList) {
        return $this->model['action']->getPrivateAcl($actionIdList, $this->getSystemId());
    }

    /* implemented */
    
    /**
     * Get action data
     * @param type $class
     * @param type $action
     * @param type $module
     * @param type $route
     * @param type $sys
     * @return array
     */
    public function getActionData($class, $action, $module, $route = null, $sys = null) {
        empty($sys) && ($sys = $this->getSystemId());
        empty($route) && ($route = implode('/', [$module, $class, $action]));
        $route = \Himax\Core::formatRoute($route);
 
        $results = $this->model['action']->getData($class, $action, $module, $route, $sys);
        return $results;
    }
    
    public function getSystemId() {
        return $this->getData('system.'.$this->model['system']->getAttr('primaryKey'));
    }
    
    protected function getSystemAlias() {
        return \HiMax\Core::getMe()->getAttr('systemAlias');
    }
    
    public function checkAccess($class, $action, $module, $route = null, $sys = null) {
        if(\HiMax\Core::getMe()->getAttr('controlLevel') !== \HiMax\Core::CONTROL_ACL) return true;
        $sys === null && $sys = $this->getSystemAlias();
        
        if ($sys === $this->getSystemAlias()) {
            return $this->_checkAccess($class, $action, $module, $route);
        }
        
        return $this->instantCheckAccess($class, $action, $module, $route, $sys);
    }
    
    public function instantCheckAccess($class, $action, $module, $route = null, $sys = null) {
        if(\HiMax\Core::getMe()->getAttr('controlLevel') !== \HiMax\Core::CONTROL_ACL) return true;
        $sys === null && $sys = $this->getSystemAlias();
        
        $actionList = $this->getActionData($class, $action, $module, $route, $sys);
        $actionLocated = $this->locateAccess($actionList);
        $checkAccess = $actionLocated
                ? $this->_checkAccess($class, $action, $module, $route, ['action'=>$actionLocated])
                : false;
        
        if($checkAccess===true) {
            if(!($profileIdList = $this->model['access']->extractProfileIdList($this->getData('user'))) && 
                    (!empty($sys) || !empty( \HiMax\Core::getMe()->getAttr('controlLevel') ))) {
                // if testing a system locally, no profile configured and control level = 0 => full access
                // if control panel is listing your list of available systems, then sys is not empty and you should have profiles configured
                return \HiMax\Core::ERROR_FORBIDDEN;
            } elseif(!empty($this->model['access']->getByProfileId_Action($actionLocated, $profileIdList))) {
                return true;
            }
        }
        return $checkAccess;
    }
    
    
}
