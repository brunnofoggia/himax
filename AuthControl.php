<?php

namespace HiMax;

/**
 * HiMax
 *
 * A toolkit implement to authenticate and authorize users actions
 *
 * @package     HiMax
 * @category	Security
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/himax
 */
trait AuthControl {
    use \DarkTrait,
            \HiMax\Login {
        \DarkTrait::getAttr as _getAttr;
        \DarkTrait::setAttr as _setAttr;
    }
    
    public function getAttr($name, $forceDefault = false) {
        return $this->_getAttr($name, $forceDefault);
    }
    
    public function setAttr($name, $value) {
        return $this->_setAttr($name, $value);
    }
    
    public function authorize() {
        if(\HiMax\Core::getMe()->getAttr('controlLevel') === \HiMax\Core::CONTROL_NONE) return true;
        
        $result = $isLogged = $this->isLogged();
        if($isLogged !== true) {
            $auth = $this->authenticate();
            if($auth !== false) {
                $result = $auth;
            }
        }
        return $result;
    }
    
    /**
     * clear useless data after getting acl
     * @access protected
     */
    protected function clearData() {
    }
    
    /**
     * Check if is there someone logged
     * @return bool
     */
    public function isLogged() {
        $user = $this->getData('user');
        return !empty($user) ? true : \HiMax\Core::ERROR_NOTAUTHENTICATED;
    }
    
    protected function authenticate() {
        $auth = false;
        if (!!($authInfo = $this->getAuthInfo())) {
            $this->getAttr('log')->add("Received info to be authenticated");
            $auth = $this->authenticateBy($authInfo);
        }
        $this->info['auth'] = $auth;
        return $auth;
    }
    
    public function authenticateBy($authInfo) {
        $this->getAttr('log')->add("info received")->add($authInfo);
        return $this->authenticateLogin($authInfo[0], $authInfo[1]);
    }

    /**
     * Authenticate user and pass
     * @access protected
     * @param user
     * @param pass
     * @return mixed true for authorized or code error
     */
    protected function authenticateLogin($user, $pass) {
        $checkLogin = $this->checkLogin($user);
        $checkPassword = $this->checkPassword($pass);

        $checkAuth = ($checkLogin !== true ? $checkLogin : ($checkPassword !== true ? $checkPassword : true));
        $checkAuth!==true && $this->eraseLoggedData();
        return $checkAuth;
    }

    /**
     * Check user sent and capture user data
     * @access protected
     * @param string $user user name
     * @return array
     */
    protected function checkLogin($user) {
        $userData = $this->getUserByLogin($user);
        $this->addLoggedData('user', $userData);

        if ($this->isLogged()!==true) {
            return \HiMax\Core::ERROR_USERNOTFOUND;
        }
        return true;
    }

    /**
     * Get user by login
     * @access protected
     * @param string $user
     * @return array
     */
    protected function getUserByLogin($user) {
        if (!empty($user) && $user === static::DEFAULT_USER) {
            return $this->defaultUser();
        }
    }
    
    protected function defaultUser() {
        return [
            'user' => static::DEFAULT_USER,
            'pass' => static::DEFAULT_PASS,
            'name' => 'Admin',
        ];
    }

    /**
     * Check password
     * @access protected
     * @param string $pass
     * @return string
     */
    protected function checkPassword($pass) {
        return $pass === static::DEFAULT_PASS ? true : \HiMax\Core::ERROR_INVALIDPASSWORD;
    }

    public function checkAccess($controller, $action, $module, $route = null) {
        return true;
    }

    /**
     * Verifies if logged user belongs to a profile that is on a higher level (is_admin) than the given level
     * @param integer $level
     * @return boolean
     */
    public function isHigherGroupThanLevel($level) {
        $userLevel = $this->isLogged()===true && !empty(@$this->getData('user')['level']) ? (int) $this->getData('user')['level'] : 0;
        
        if($level !== '0' && empty($level)) $level = 0;
        
        if ((int) $userLevel > (int) $level || (int) $userLevel === 9) return true;
        return false;
    }

}